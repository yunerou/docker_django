#!/bin/sh

echo "waiting for database..." $DATABASE
./wait_for_it.sh -h $SQL_HOST -p $SQL_PORT

# python manage.py flush --no-input
# python manage.py migrate
# python manage.py collectstatic --no-input --clear

exec "$@"